#include <stdio.h>
#include <stdlib.h>

void dump(unsigned char t[], int nb)
{
    int i, j;

    for (i = 0; i < nb; i += 16)
    {
        // Affiche le num�ro du premier octet de la ligne en d�cimal
        printf("%04d: ", i);

        // Affiche les octets en hexad�cimal (deux chiffres hexa par octet)
        for (j = 0; j < 16 && i + j < nb; ++j)
        {
            printf("%02X ", t[i + j]);
        }

        // Ajoute des espaces suppl�mentaires si la ligne est incompl�te
        for (j = 0; j < 16; ++j)
        {
            printf("   ");
        }

        printf("\n");
    }
}



// Fonction de compression
int compression(unsigned char source[], int nb, unsigned char dest[])
{
    int i, j;
    int destIndex = 0;

    for (i = 0; i < nb; ++i)
    {
        // V�rifier si l'octet actuel est un octet d'�chappement
        if (source[i] == 0xFE || source[i] == 0xFD)
        {
            dest[destIndex++] = 0xFD; // Octet d'�chappement
            dest[destIndex++] = source[i]; // Octet original
        }
        else
        {
            // Compter la s�rie d'octets identiques
            int compteur = 1;
            while (i + 1 < nb && source[i] == source[i + 1])
            {
                ++compteur;
                ++i;
            }

            // Si la s�rie est plus grande que 3 octets, compresser la s�rie
            if (compteur > 3)
            {
                dest[destIndex++] = 0xFE; // Octet de contr�le pour d�but de s�rie compress�
                dest[destIndex++] = compteur; // Nombre de r�p�titions
                dest[destIndex++] = source[i]; // Octet original
            }
            else
            {
                // Sinon, copier simplement les octets
                for (j = 0; j < compteur; ++j)
                {
                    dest[destIndex++] = source[i];
                }
            }
        }
    }

    return destIndex;
}

// Fonction de d�compression
int decompression(unsigned char source[], int nb, unsigned char dest[])
{
    int i, j;
    int destIndex = 0;

    for (i = 0; i < nb; ++i)
    {
        // V�rifier si l'octet actuel est un octet d'�chappement
        if (source[i] == 0xFD)
        {
            // Octet d'�chappement, copier l'octet suivant sans v�rification
            dest[destIndex++] = source[++i];
        }
        else if (source[i] == 0xFE)
        {
            // D�but de s�rie compress�
            int compteur = source[++i]; // Nombre de r�p�titions
            unsigned char valeur = source[++i]; // Octet original

            // Copier la s�rie d�compress�e
            for (j = 0; j < compteur; ++j)
            {
                dest[destIndex++] = valeur;
            }
        }
        else
        {
            // Copier simplement les octets
            dest[destIndex++] = source[i];
        }
    }

    return destIndex;
}

