#ifndef COMPRESSION_H_INCLUDED
#define COMPRESSION_H_INCLUDED

void dump(unsigned char t[], int nb);
int compression(unsigned char source[], int nb, unsigned char dest[]);
int decompression(unsigned char source[], int nb, unsigned char dest[]);

#endif // COMPRESSION_H_INCLUDED
