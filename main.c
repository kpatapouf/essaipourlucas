#include <stdio.h>

#define BUFFER_SIZE 10

int buffer[BUFFER_SIZE];
int start = 0;
int end = 0;
int count = 0;

void add_to_buffer(int value) {
    buffer[end] = value;
    end = (end + 1) % BUFFER_SIZE;
    if (count == BUFFER_SIZE) {
        start = (start + 1) % BUFFER_SIZE; // remove the oldest value when buffer is full
    } else {
        count++;
    }
}

int get_from_buffer() {
    if (count == 0) {
        printf("Buffer is empty\n");
        return -1;
    } else {
        int value = buffer[start];
        start = (start + 1) % BUFFER_SIZE;
        count--;
        return value;
    }
}

int main() {
    // Test the circular buffer
    for (int i = 0; i < 12; i++) {
        add_to_buffer(i);
    }
    for (int i = 0; i < 10; i++) {
        printf("%d\n", get_from_buffer());
    }
    return 0;
}
